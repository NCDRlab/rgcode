from os import path
from tifffile import TiffFile
from skimage.transform import rescale, resize
from skimage.exposure import rescale_intensity
from skimage.measure import label, regionprops
from skimage.draw import circle
from skimage.morphology import binary_dilation, disk, remove_small_objects
from skimage.segmentation import find_boundaries
# from matplotlib import rcParams
# from keras_unet.utils import get_patches, plot_patches
from skimage.util import view_as_windows, view_as_blocks, montage, crop, img_as_float, img_as_ubyte
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
import numpy as np
import pandas as pd

class Retina(object):

    def __init__(self, file_path, resolution=None):
        
        # sets some useful shortcuts
        self.full_path = file_path
        self.folder, self.full_filename = path.split(self.full_path)
        self.filename, self.ext = path.splitext(self.full_filename)

        self.resolution = resolution

        # defines the target scale for the model
        self.__target_res = 2.1672 / 2

        # loads in the actual image
        self.load_image()

    def load_image(self):

        print(f"Loading {self.full_filename}")

        # loading the picture and storing the metadata
        with TiffFile(self.full_path) as tif:
            if self.resolution == None:
                self.x_resolution = tif.pages[0].tags['XResolution'].value[0] / 1000000
                self.y_resolution = tif.pages[0].tags['YResolution'].value[0] / 1000000
            else:
                self.x_resolution = self.resolution
                self.y_resolution = self.resolution
            image = img_as_float(tif.asarray())
            image = rescale_intensity(image)
            self.original_image = image

            # rescales to the target scale
            if (np.round(self.x_resolution, decimals=1) != np.round(self.__target_res, decimals=1)) or (np.round(self.y_resolution, decimals=1) != np.round(self.__target_res, decimals=1)):
                image = self.rescale_to_target(image)

            self.image = image
            self.shape = image.shape
            return image
    

    def rescale_to_target(self, image, reverse=False, anti_aliasing=True):

        # calculates rescale factor
        x_rescale_factor = self.__target_res / self.x_resolution
        y_rescale_factor = self.__target_res / self.y_resolution

        # if upscaling take the reciprocal
        if reverse is True:
            x_rescale_factor = 1 / x_rescale_factor
            y_rescale_factor = 1 / y_rescale_factor

        return rescale(image, (x_rescale_factor, y_rescale_factor), anti_aliasing=anti_aliasing)
    

    def get_blocks(self, block_size=128, image_size=None, overlap=0.125, rescale_int=False):

        # gets the image
        if image_size == None:
            img = self.image
        else:
            # img = resize(self.image, image_size)

            img = rescale_intensity(self.image)

            shape = np.array(img.shape)

            ratio = shape.min() / shape.max()

            new_shape = np.copy(shape)
            if shape[0] == shape[1]:
                new_shape = image_size
            else:
                new_shape[shape.argmax()] = image_size[0]
                new_shape[shape.argmin()] = image_size[0] * ratio

            img = resize(img, new_shape)


        # sets measurements

        # 12,5% overlap. Works well with powers of 2
        overlap = int(block_size * overlap)
        step_size = block_size - overlap

        # compansating for overlap. Will remove half of the overlap
        right_pad = int(overlap / 2)
        self.right_pad = right_pad

        # this pads the image so it generates an extra set of blocks on the right with the
        # otherwise missing part of the picture
        shape = np.array(img.shape)
        self.shape = shape
        padded_shape = (np.ceil(shape / step_size) * step_size).astype(int)
        left_pad = padded_shape - shape + overlap + right_pad

        # applies the padding
        img = np.pad(img, pad_width=[(right_pad, left_pad[0]), (right_pad, left_pad[1])], mode='constant', constant_values=0)

        # generates overlapping patches
        blocks = view_as_windows(img, (block_size, block_size), step=step_size)
        new_shape = (blocks.shape[0], blocks.shape[1])

        # flattens to a 3D array
        blocks = np.reshape(blocks, (blocks.shape[0] * blocks.shape[1], blocks.shape[2], blocks.shape[3]))

        if rescale_int is True:
            blocks = np.array([rescale_intensity(block) for block in blocks])

        return blocks, new_shape
    

    def make_overlay(self):

        # creates an image with dots on the cell's centorids
        dot_image = np.zeros(self.prediction.shape)

        # sets the radius of the dots
        # radius = int(2 * self.x_resolution / self.__target_res)
        radius = int(np.round((4 / 2.17) * self.x_resolution, 0))

        # draws circels centred around the centroids
        for coords in self.centroids:
            rr, cc = circle(coords[0], coords[1], radius, shape=dot_image.shape)
            dot_image[rr, cc] = 1
        
        dot_image = img_as_ubyte(dot_image > 0.5)
        self.dot_image = dot_image

        # creates an empty blue chennel
        blue = np.zeros(self.original_image.shape, dtype=np.ubyte)

        # stacks the pictures in an RGB one
        overlay = np.dstack([self.dot_image, img_as_ubyte(self.original_image), blue])

        # makes a contour of the retina if available
        try:
            self.make_contour()
            overlay = np.maximum(self.contour, overlay)
            self.overlay = img_as_ubyte(overlay)
        except:
            self.overlay = img_as_ubyte(overlay)

        return img_as_ubyte(overlay)
    

    def make_isodensity(self, cmap, bandwidth):

        # TODO set DPI and COLORMAP
        
        # Set maximum density
        max_density = 6000
        cell_count = self.count
        vmax = float(max_density * 10 / cell_count / 10000000)
        n_levels = 12
        # print(f"vmax: {vmax}")


        centroids = self.centroids * (1/self.x_resolution)


        fig, ax = plt.subplots(figsize=(6, 5))
        sns.kdeplot(centroids[:, 1], np.flip(centroids[:, 0]), cbar=False, legend=False, shade=True, shade_lowest=False, cmap=cmap, vmax=vmax, bw=bandwidth, n_levels=n_levels)

        # Sets scalebar converting the probability function to actual density
        scale_max = vmax / 10 * cell_count * 10000000
        ###print("density", scale_max)
        linspace = np.linspace(0, scale_max, n_levels + 1)
        # print(linspace)
        m = plt.cm.ScalarMappable(cmap=cmap)
        m.set_array([0, scale_max])
        m.set_clim(0., scale_max)
        plt.colorbar(m, ticks=np.linspace(0, scale_max, n_levels + 1))
        # fig.axes[1].set_yticklabels(np.linspace(0, max_density * 1.1 , 11, dtype=np.int))

        # Set x,y labels
        label = "Position (µm)"
        fig.axes[0].set_xlabel(label)
        fig.axes[0].set_ylabel(label)

        fig.axes[0].axis('off')

        # Set scale label
        fig.axes[1].set_ylabel("Density (cells/mm$\mathregular{^{2}}$)")

        return fig
    

    def make_contour(self):

        # finds the contour of the retina
        contour = find_boundaries(self.segmentation)
        contour = binary_dilation(contour, selem=disk(5))

        # stacks the contour so it can be blended in an overlay
        white_contour = np.dstack([contour, contour, contour])

        self.contour = img_as_ubyte(white_contour)

        return white_contour
    

    def get_properties(self):

        # clean based on the segmentation, if available
        try:
            # get the toal segmented area
            total_area = np.count_nonzero(self.segmentation)

            # remove objects that are less than 2,5% of the toal area
            area_threshold = total_area * 0.025
            clean_segmentation = remove_small_objects(self.segmentation, min_size=area_threshold)

            # get the area and converts it to mm^2
            self.area = np.count_nonzero(clean_segmentation) * (1/self.x_resolution) * (1/self.y_resolution) / 1000000

            # delete what falls outside of the segmentation
            segmented_prediction = self.prediction
            segmented_prediction[~clean_segmentation] = False
        except:
            segmented_prediction = self.prediction
            clean_segmentation = np.ones(self.probability_map.shape)
            self.area = np.count_nonzero(clean_segmentation) * (1/self.x_resolution) * (1/self.y_resolution) / 1000000

        # labels the binary image and gets objects
        label_image = label(segmented_prediction)
        regions = regionprops(label_image)
        centroids = [region.centroid for region in regions]
        self.centroids = np.array(centroids).astype(int)

        # np.save("centroids.npy", self.centroids)

        # stores the count and overrides prediction and segmentation
        self.count = len(regions)
        self.density = np.round(self.count / self.area, 2)
        self.segmentaton = clean_segmentation
        self.prediction = segmented_prediction

        return self.count

